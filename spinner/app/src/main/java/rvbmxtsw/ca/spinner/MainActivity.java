package rvbmxtsw.ca.spinner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity {

    Spinner spinner;
    Switch btnSwitch;
    String[] professionList = {"Kinesiology", "Physiotherapy", "Chiropractor", "Massage Therapist ", "Dietitian", "Naturopathy"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = (Spinner) findViewById(R.id.spinner);
        btnSwitch = (Switch) findViewById(R.id.switch1);

        //<String> adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_item, professionList);
        //spinner.setAdapter(adapter);

        btnSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                    spinner.setVisibility(View.VISIBLE);
                } else {
                    spinner.setVisibility(View.GONE);
                }
            }
        });

    }
}
