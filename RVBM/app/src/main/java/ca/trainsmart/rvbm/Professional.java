package ca.trainsmart.rvbm;

import java.io.Serializable;

public class Professional implements Serializable {
    String field;

    public Professional(String field) {
        this.field = field;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}
