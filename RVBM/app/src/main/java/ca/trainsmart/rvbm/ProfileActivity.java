package ca.trainsmart.rvbm;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.HashMap;

public class ProfileActivity extends AppCompatActivity {

    private static final String TAG = "State";
    private static final int PICK_PROFILE_PICTURE = 1111;

    private ImageView profilePic;
    private EditText txtName, txtDesc;
    private String profilePicURL;
    private Uri uriProfilePicture;
    private FirebaseUser user;
    private FirebaseAuth mAuth;
    private DatabaseReference dbRefProfilePics, dbRefUserInfo;
    private String currentUID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        currentUID = mAuth.getCurrentUser().getUid();
        dbRefProfilePics = FirebaseDatabase.getInstance().getReference().child("ProfilePics").child(currentUID);
        dbRefUserInfo = FirebaseDatabase.getInstance().getReference().child("userInfo").child(currentUID);



        profilePic = (ImageView) findViewById(R.id.imageView);
        txtName = (EditText) findViewById(R.id.txtName);
        txtDesc = (EditText) findViewById(R.id.txtDescription);

        findViewById(R.id.btnSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveProfileInfo();
            }
        });

        //Select Profile picture
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImagePicker();
            }
        });

        //getUserInfo();

        Log.i(TAG, "ProfileActivity.onCreate");

    }

    private void showImagePicker() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Profile Picture"), PICK_PROFILE_PICTURE);
        Log.i(TAG, "ProfileActivity.showImagePicker");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i(TAG, "ProfileActivity.onActivityResult");

        if(requestCode == PICK_PROFILE_PICTURE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            uriProfilePicture = data.getData();
                try {
                    Log.i(TAG, "ProfileActivity.onActivityResult.uriProfilePic:" + uriProfilePicture.toString());
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uriProfilePicture);
                profilePic.setImageBitmap(bitmap);
                    Log.i(TAG, "ProfileActivity.onActivityResult.uriProfilePic:" + profilePic.toString());
                uploadImageFirebase();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
    //Upload to Firebase storage
    private void uploadImageFirebase() {

        Log.i(TAG, "ProfileActivity.uploadImageFirebase");
        StorageReference profilePictureRef = FirebaseStorage.getInstance().getReference("img/userProfilePictures/"+System.currentTimeMillis() + ".jpg");
        if(uriProfilePicture != null) {
            profilePictureRef.putFile(uriProfilePicture).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    //Get download urk
                    profilePicURL = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
                    Log.i(TAG, "ProfileActivity.uploadImageFirebase.!=null"+ profilePicURL);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(ProfileActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void saveProfileInfo() {
        Log.i(TAG, "ProfileActivity.saveProfileInfo");
        Log.i(TAG, "ProfileActivity.saveProfileInfo.photo:" +
            FirebaseDatabase.getInstance("gs://rvbmxtsw-8f5e5.appspot.com/img/userProfilePictures/")
        );
        String userName = txtName.getText().toString();
        String userDesc = txtDesc.getText().toString();
        if (userName.isEmpty()) {
            Log.i(TAG, "ProfileActivity.saveProfileInfo.userName.isEmpty");
            txtName.setError("Name required");
            txtName.requestFocus();
            return;
        } else {
            Log.i(TAG, "ProfileActivity.saveProfileInfo.Save");
            /*
            FirebaseDatabase.getInstance().getReference("ProfilePics").child(currentUID).setValue("imageName", "test.jpg");
            FirebaseDatabase.getInstance().getReference("ProfilePics").child(currentUID).setValue("uid", currentUID);
            FirebaseDatabase.getInstance().getReference("ProfilePics").child(currentUID).setValue("user", userName);

            FirebaseDatabase.getInstance().getReference("userInfo").child(currentUID).setValue("aboutMe", userDesc);
            FirebaseDatabase.getInstance().getReference("userInfo").child(currentUID).setValue("Weight", "none");
            FirebaseDatabase.getInstance().getReference("userInfo").child(currentUID).setValue("Height", "none");
            FirebaseDatabase.getInstance().getReference("userInfo").child(currentUID).setValue("Age", "none");
             */

            HashMap profilePics = new HashMap();
            profilePics.put("imageName", uriProfilePicture.toString());
            profilePics.put("uid", currentUID);
            profilePics.put("user", userName);

            HashMap userInfo = new HashMap();
            userInfo.put("aboutMe", userDesc);
            userInfo.put("weight", "none");
            userInfo.put("height", "none");
            userInfo.put("age", "none");


            dbRefProfilePics.setValue(profilePics);
            dbRefUserInfo.setValue(userInfo);

            dbRefProfilePics.updateChildren(profilePics).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if(task.isSuccessful()) {
                        Toast.makeText(ProfileActivity.this, "Account created successfully", Toast.LENGTH_SHORT).show();
                        Log.i(TAG, "ProfileActivity.saveProfileInfo.save.complete");
                    }
                }
            });

        }


        if(user!=null && profilePicURL != null) {
            UserProfileChangeRequest profile = new UserProfileChangeRequest.Builder()
                    .setDisplayName(userName)
                    .setPhotoUri(Uri.parse(profilePicURL))
                    .build();

            user.updateProfile(profile).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(ProfileActivity.this, "Profile updated", Toast.LENGTH_SHORT).show();
                        finish();
                        startActivity(new Intent(ProfileActivity.this, ViewProfileActivity.class));

                    } else {
                        Toast.makeText(ProfileActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else if (profilePicURL == null){
            Toast.makeText(ProfileActivity.this, "profilePicURL is null", Toast.LENGTH_SHORT).show();
        }
    }

    /*
    private void getUserInfo() {
        if (user != null) {
            if (user.getPhotoUrl() != null) {
                Toast.makeText(ProfileActivity.this, "Setting fields", Toast.LENGTH_SHORT).show(); //Debug step
                Glide.with(this)
                        .load(user.getPhotoUrl().toString())
                        .into(profilePic);

            } else {
                Toast.makeText(ProfileActivity.this, "PhotoURL null", Toast.LENGTH_SHORT).show();
            }
            if (user.getDisplayName() != null) {
                txtName.setText(user.getDisplayName());
            }
        }
    }
    */
}
