package ca.trainsmart.rvbm;

public class User {

    String name, email, uid;
    int type;
    Professional field;
    Info accountInfo;

    public User() {

    }

    public User(String uid, String name, String email, int type) {
        this.uid = uid;
        this.name = name;
        this.email = email;
        this.type = type;

    }

    public User(String name, String email, String uid, int type, Professional field) {
        this.name = name;
        this.email = email;
        this.uid = uid;
        this.type = type;
        this.field = field;
    }

    public User(String name, String email, String uid, Info accountInfo) {
        this.name = name;
        this.email = email;
        this.uid = uid;
        this.accountInfo = accountInfo;
    }

    public Professional getField() {
        return field;
    }

    public void setField(Professional field) {
        this.field = field;
    }

    public String getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public int getType() {
        return type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setType(int type) {
        this.type = type;
    }
}
