package ca.trainsmart.rvbm;

public class Info {

    String accountType;
    String age;
    String bio;
    String name;
    String user;

    public Info(String accountType, String age, String bio, String name, String user) {
        this.accountType = accountType;
        this.age = age;
        this.bio = bio;
        this.name = name;
        this.user = user;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
