package ca.trainsmart.rvbm;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.util.Calendar;

public class UserSetupActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private EditText txtName, txtEmail;
    private LinearLayout profLayout, patientLayout;
    private Switch userType;
    private Button btnFinish;
    private Spinner spinner;
    private int userTypeInt = 0;
    private String[] professionList = {"Kinesiology", "Physiotherapy", "Chiropractor", "Massage Therapist ", "Dietitian", "Naturopathy"};
    private TextView mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDateSetListener;

    DatabaseReference databaseRef;
    FirebaseAuth mAuth;

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);

        String date = DateFormat.getDateInstance().format(cal.getTime());
        mDisplayDate.setText(date);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_setup);

        txtName = (EditText) findViewById(R.id.txtNameSetup);
        txtEmail = (EditText) findViewById(R.id.txtEmailSetup);
        userType = (Switch) findViewById(R.id.swUserType);
        profLayout = (LinearLayout) findViewById(R.id.llProf);
        patientLayout = (LinearLayout) findViewById(R.id.llPatient);
        spinner = (Spinner) findViewById(R.id.sProfession);
        btnFinish = (Button) findViewById(R.id.btnSubmit);
        mDisplayDate = (TextView) findViewById(R.id.DoB);

        databaseRef = FirebaseDatabase.getInstance().getReference("user");
        mAuth = FirebaseAuth.getInstance();

        Intent intent = getIntent();
        final String email = intent.getStringExtra("Email");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(UserSetupActivity.this, android.R.layout.simple_spinner_item, professionList);
        spinner.setAdapter(adapter);
        txtEmail.setText(email);


        userType.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    profLayout.setVisibility(View.VISIBLE);
                    patientLayout.setVisibility(View.GONE);
                    spinner.setVisibility(View.VISIBLE);
                } else {
                    profLayout.setVisibility(View.GONE);
                    patientLayout.setVisibility(View.VISIBLE);
                    spinner.setVisibility(View.GONE);
                }
            }
        });


        mDisplayDate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                DialogFragment datePicker = new ca.trainsmart.rvbm.DatePicker();
                datePicker.show(getSupportFragmentManager(), "Date Picker");
            }
        });


        /*
        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int mYear = cal.get(Calendar.YEAR);
                int mMonth = cal.get(Calendar.MONTH);
                int mDay = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        UserSetupActivity.this, R.style.Theme_AppCompat_Light_Dialog_MinWidth, mDateSetListener, mYear,mMonth,mDay
                );
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                String date = month + "/" + day + "/" + year;
                mDisplayDate.setText(date);
            }
        };

        */

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = txtName.getText().toString().trim();
                String field = spinner.getSelectedItem().toString();
                if(!TextUtils.isEmpty(name)) {
                    if (userType.isChecked()) {
                        userTypeInt = 1;
                    } else {
                        userTypeInt = 2;
                    }

                    String id = mAuth.getCurrentUser().getUid();
                    User user;
                    Info accountInfo;
                    String age;
                    if(userTypeInt == 1) {
                        age = mDisplayDate.getText().toString();
                        accountInfo = new Info("Health Professional", age, "bio", name, email);
                        user = new User(id, name, email, accountInfo);
                        databaseRef.child(id).setValue(user);
                        Toast.makeText(UserSetupActivity.this, "Professional added", Toast.LENGTH_SHORT).show();

                    } else if(userTypeInt == 2) {
                         user = new User(id, name, email, 2);
                    } else {
                        user = new User(id,"null", email, 0);
                    }
                    databaseRef.child(id).setValue(user);
                    Toast.makeText(UserSetupActivity.this, "Patient added", Toast.LENGTH_SHORT).show();
                    UserSetupActivity.this.finish();
                    startActivity(new Intent(UserSetupActivity.this, ViewProfileActivity.class));
                } else {
                    txtName.setError("Name is required");
                    txtName.requestFocus();
                    return;
                }
            }
        });
    }
}

