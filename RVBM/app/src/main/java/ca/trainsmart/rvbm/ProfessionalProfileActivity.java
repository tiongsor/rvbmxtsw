package ca.trainsmart.rvbm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ProfessionalProfileActivity extends AppCompatActivity {

    private static final String TAG = "State";

    private ImageView profilePic;
    private TextView lblName, lblDesc;
    private Button btnLogout, btnEdit, btnHome;

    DatabaseReference databaseRef;
    FirebaseAuth mAuth;
    FirebaseUser mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_professional_profile);

        profilePic = (ImageView) findViewById(R.id.ivProfilePic);
        lblName = (TextView) findViewById(R.id.lblName);
        lblDesc = (TextView) findViewById(R.id.lblDescription);
        btnLogout = (Button) findViewById(R.id.btnLogout);
        btnEdit = (Button) findViewById(R.id.btnEdit);
        btnHome = (Button) findViewById(R.id.btnHome);

        databaseRef = FirebaseDatabase.getInstance().getReference("user");
        mAuth  = FirebaseAuth.getInstance();

        Log.i(TAG, "ProfessionalProfileActivity.onCreate");

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "ViewProfileActivity.onCreate.logoutBtn");
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(ProfessionalProfileActivity.this, LoginActivity.class));
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "ViewProfileActivity.onCreate.editBtn");
                startActivity(new Intent(ProfessionalProfileActivity.this, ProfileActivity.class));
            }
        });

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "ViewProfileActivity.onCreate.homeBtn");
                startActivity(new Intent(ProfessionalProfileActivity.this, HomeActivity.class));
            }
        });

    }
}
