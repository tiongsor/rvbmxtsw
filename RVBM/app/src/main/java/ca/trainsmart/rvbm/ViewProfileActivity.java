package ca.trainsmart.rvbm;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ViewProfileActivity extends AppCompatActivity {

    private static final String TAG = "State";

    private ImageView profilePic;
    private TextView lblName, lblDesc;
    private Button btnLogout, btnEdit, btnHome;

    DatabaseReference databaseRef;
    FirebaseAuth mAuth;
    FirebaseUser mUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);

        profilePic = (ImageView) findViewById(R.id.ivProfilePic);
        lblName = (TextView) findViewById(R.id.lblName);
        lblDesc = (TextView) findViewById(R.id.lblDescription);
        btnLogout = (Button) findViewById(R.id.btnLogout);
        btnEdit = (Button) findViewById(R.id.btnEdit);
        btnHome = (Button) findViewById(R.id.btnHome);

        databaseRef = FirebaseDatabase.getInstance().getReference("user");
        mAuth  = FirebaseAuth.getInstance();

        Log.i(TAG, "ViewProfileActivity.onCreate");

        //Logout button
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "ViewProfileActivity.onCreate.logoutBtn");
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(ViewProfileActivity.this, LoginActivity.class));
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "ViewProfileActivity.onCreate.editBtn");
                startActivity(new Intent(ViewProfileActivity.this, ProfileActivity.class));
            }
        });

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "ViewProfileActivity.onCreate.homeBtn");
                startActivity(new Intent(ViewProfileActivity.this, HomeActivity.class));
            }
        });

        getUserInfo();
        Log.i(TAG, "ViewProfileActivity.onCreate.getUserInfo");
    }

    @Override
    protected void onStart() {
        Log.i(TAG, "ViewProfileActivity.onStart");
        super.onStart();
        if(mAuth.getCurrentUser() == null) {
            finish();
            startActivity(new Intent(ViewProfileActivity.this, LoginActivity.class));
         }
    }

    private void getUserInfo() {

        Log.i(TAG, "ViewProfileActivity.getUserInfo");
        mUser = mAuth.getCurrentUser();

        if (mUser != null) {
            Log.i(TAG, "ViewProfileActivity.getUserInfo.mUser!=null");
            if (mUser.getPhotoUrl() != null) {
                Log.i(TAG, "ViewProfileActivity.getUserInfo.mUser.getPhoto!=null");
                Log.i(TAG, "uID: \"" + mUser.getUid() +
                        "\"");
                Log.i(TAG, "ProviderId: \"" + mUser.getProviderId() +
                        "\"");
                Log.i(TAG, "DisplayName: \"" + mUser.getDisplayName() +
                        "\"");
                Log.i(TAG, "Email: \"" + mUser.getEmail() +
                        "\"");
                Log.i(TAG, "Phone: \"" + mUser.getPhoneNumber() +
                        "\"");
                Log.i(TAG, "hashCode: \"" + mUser.hashCode() +
                        "\"");

                Toast.makeText(ViewProfileActivity.this, "Setting fields", Toast.LENGTH_SHORT).show(); //Debug step
                Glide.with(this)
                        .load(mUser.getPhotoUrl().toString())
                        .into(profilePic);
            } else {
                Toast.makeText(ViewProfileActivity.this, "PhotoURL null", Toast.LENGTH_SHORT).show();
            }
            if (mUser.getDisplayName() != null) {
                Log.i(TAG, "ViewProfileActivity.getUserInfo.mUser.getDisplayName");
                lblName.setText(mUser.getDisplayName());
            }
        }
    }


}
