package ca.trainsmart.rvbm;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "State";
    private FirebaseAuth mAuth;
    private EditText txtEmail, txtPassword;
    private Button btnLogin;


    @Override
    protected void onStart() { //If user is already logged in, skip login screen and got to profile
        super.onStart();
        Log.i(TAG, "LoginActivity.onStart");
        if(mAuth.getCurrentUser() != null) {
            finish();
            startActivity(new Intent(this, ViewProfileActivity.class));
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        //Initialize
        mAuth = FirebaseAuth.getInstance();

        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        //OnClickListeners --Signup --Login --Google Login
        findViewById(R.id.lblSignup).setOnClickListener(this);
        findViewById(R.id.btnLogin).setOnClickListener(this);
        Log.i(TAG, "LoginActivity.onCreate");
    }



    @Override
    public void onClick(View v) {
        Log.i(TAG, "LoginActivity.onClick");
        switch(v.getId()) {
            case R.id.lblSignup:
                finish();
                startActivity(new Intent(this, SignUpActivity.class));
                break;
            case R.id.btnLogin:
                userLogin();
                break;
        }
    }

    public void userLogin() {
        String email = txtEmail.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();

        Log.i(TAG, "LoginActivity.userLogin");
        //Validate inputs
        if (email.isEmpty()) {
            txtEmail.setError("Email is required");
            txtEmail.requestFocus();
            return;
        }
        if(password.isEmpty()) {
            txtPassword.setError("Password is required");
            txtPassword.requestFocus();
            return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {  //Invalid email input
            txtEmail.setError("Please enter a valid email");
            txtEmail.requestFocus();
            return;
        }

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()) {
                    //User logs in
                    finish();
                    Intent intent = new Intent(LoginActivity.this, ViewProfileActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}

