package ca.trainsmart.rvbm;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "State";
    EditText txtEmail, txtPassword;
    TextView lblLogin;
    Button btnSignup;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mAuth = FirebaseAuth.getInstance();

        //Initialize inputs
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnSignup = (Button) findViewById(R.id.btnSignup);
        lblLogin = (TextView) findViewById(R.id.lblLogin);

        //OnClickListeners --Signup --Login
        btnSignup.setOnClickListener(this);
        lblLogin.setOnClickListener(this);

        Log.i(TAG, "SignUpActivity.onCreate");
    }

    @Override
    public void onClick(View v) {
        Log.i(TAG, "SignUpActivity.onClick");
        switch (v.getId()) {
            case R.id.btnSignup: //OnClick Signup
                createUser();
                break;
            case R.id.lblLogin: //Go back to login page
                finish();
                startActivity(new Intent(this, LoginActivity.class));
                break;
        }
    }

    private void createUser() {
        final String email = txtEmail.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();

        Log.i(TAG, "SignUpActivity.createUser");

        //Validate inputs
        if (email.isEmpty()) {
            txtEmail.setError("Email is required");
            txtEmail.requestFocus();
            return;
        }
        if(password.isEmpty()) {
            txtPassword.setError("Password is required");
            txtPassword.requestFocus();
            return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {  //Invalid email input
            txtEmail.setError("Please enter a valid email");
            txtEmail.requestFocus();
            return;
        }
        if(password.length()<6) { //Password requirements not met
            txtPassword.setError("Password does not meet minimum length of 6 characters");
            txtPassword.requestFocus();
            return;
        }

        //Create user account
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()) { // If task is successful
                    Toast.makeText(getApplicationContext(), "User registration successful", Toast.LENGTH_SHORT).show();
                    //User logs in
                    finish();
                    startActivity(new Intent(SignUpActivity.this, UserSetupActivity.class).putExtra("Email", email));
                } else {
                    if(task.getException() instanceof FirebaseAuthUserCollisionException) {  //Error if user is already registered
                        txtEmail.setError("Email is already registered");
                        txtEmail.requestFocus();
                    } else {
                        Toast.makeText(getApplicationContext(), "An Error occurred, please try again.", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
    }
}
